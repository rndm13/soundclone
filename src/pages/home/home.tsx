import SideBarLayout from "../../ui/sidebar/sidebar";
import { useLayoutEffect, useRef, useState } from "react";
import "./home.scss";

function Album({
  name,
  artist,
  thumbnail,
}: {
  name: string;
  artist: string;
  thumbnail: string;
}) {
  return (
    <div className="album">
      <p>{name}</p>
      <p>By {artist}</p>
      <img src={thumbnail} alt="Thumbnail" />
    </div>
  );
}

function Trending() {
  return (
    <div className="trending">
      <div className="trending__panel">
        <h2>Trending</h2>
        <h3 className="trending__genre_silenced">Rock</h3>
        <h3 className="trending__genre">Electronic</h3>
        <h3 className="trending__genre_silenced">Metal</h3>
        <h3 className="trending__genre_silenced">Country</h3>
        <h3 className="trending__genre_silenced">Indie</h3>
        <button className="trending__toggle-playlists">Toggle Playlists</button>
      </div>
      {
        // TOOD: add a scroll fadeout
      }
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
      <Album
        name="Cross"
        artist="Justice"
        thumbnail="/albums/justice/cross/cover.jpg"
      />
    </div>
  );
}

function SoundVisualization() {
  const dimensionRef = useRef<any>();
  const [dimensions, setDimensions] = useState({ width: 0, height: 0 });
  useLayoutEffect(() => {
    if (dimensionRef.current) {
      setDimensions({
        width: dimensionRef.current.offsetWidth,
        height: dimensionRef.current.offsetHeight,
      });
    }
  }, []);

  const blocks = [];
  const block_count = 20;
  const block_gap = 5;
  const block_width = (dimensions.width - (block_count + 1) * block_gap) / block_count;
  const block_height = (dimensions.height - (block_count + 1) * block_gap) / block_count;
  for (var i = 0; i < block_count; i++) {
    const max_j = Math.trunc(Math.random() * block_count);
    for (var j = max_j; j < block_count; j++) {
      blocks.push(
        <rect
          key={i + j * block_count}
          x={block_gap * (i + 1) + block_width * i}
          y={block_gap * (j + 1) + block_height * j}
          width={block_width}
          height={block_height}
          fill="white"
        />,
      );
    }
  }
  return (
    <div className="sound-visualization">
      <h2>Sound Visualization</h2>
      <div className="sound-visualization__blocks" ref={dimensionRef}>
        <svg viewBox="0 0 10 10">
          <mask id="block-mask">{blocks}</mask>
        </svg>
      </div>
    </div>
  );
}

export default function Home() {
  return (
    <>
      <header></header>
      <meta title="SoundClone | Home" />
      <SideBarLayout>
        <main className="home">
          <Trending />
          <SoundVisualization />
        </main>
      </SideBarLayout>
    </>
  );
}
