import "./search.scss";
export default function Search() {
  return (
    <div className="search">
      <input
        type="search"
        name="search"
        placeholder="Search..."
        className="search__input"
      />
      <img src="/search.svg" alt="Magnifying glass icon" className="search__icon"/>
    </div>
  );
}
