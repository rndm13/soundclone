import { PropsWithChildren, useState } from "react";
import "./sidebar.scss";
import Avatar from "../avatar/avatar";
import Search from "../search/search";

// children are your main view
export default function SideBarLayout({ children }: PropsWithChildren) {
  return (
    <div className="sidebar">
      <SideBar />
      <div className="sidebar__main">{children}</div>
    </div>
  );
}

function Track({
  name,
  artist,
  thumbnail,
  children,
}: PropsWithChildren<{ name: string; artist: string; thumbnail: string }>) {
  return (
    <div className="track">
      <img src={thumbnail} alt="Thumbnail" className="track__thumbnail" />
      <div className="track__texts">
        <h3>{name}</h3>
        <h3 className="text_silenced">{artist}</h3>
      </div>
      {children}
    </div>
  );
}

function toTime(seconds: number): string {
  return `${Math.trunc(seconds / 60)}:${(seconds % 60)
    .toString()
    .padStart(2, "0")}`;
}

function SongDuration({
  duration,
  startTime = 0,
}: {
  duration: number;
  startTime: number;
}) {
  const [time, setTime] = useState(startTime);
  const current = (time / duration) * 100;

  return (
    <div className="song-duration">
      <div className="song-duration__time-control">
        <input
          type="range"
          value={time}
          onChange={(e) => setTime(parseInt(e.target.value))}
          min={0}
          max={duration}
        />
        <div
          className="popover"
          style={{
            left: `calc(${current}% - (${current / 100 - 1} * 20px) - 7.5px)`,
          }}
        >
          <p>{toTime(time)}</p>
        </div>
      </div>
      <div className="song-duration__total">
        <p>{toTime(duration)}</p>
      </div>
    </div>
  );
}

function PlayPauseToggle() {
  const [play, setPlay] = useState(true);

  return (
    <button className="track__start-end" onClick={() => setPlay((old) => !old)}>
      {play ? (
        <img src="/pause.svg" alt="Pause" />
      ) : (
        <img src="/start.svg" alt="Start" />
      )}
    </button>
  );
}

function VolumeControl() {
  const [volume, setVolume] = useState(50);

  return (
    <div className="track__volume">
      {volume == 0 ? (
        <img src="/volume_off.svg" alt="Volume mid" />
      ) : (
        <img src="/volume_mid.svg" alt="Volume mid" />
      )}
      <div className="popover">
        <input
          type="range"
          value={volume}
          onChange={(e) => setVolume(parseInt(e.target.value))}
          min={0}
          max={100}
        />
        <div
          className="popover"
          style={{
            left: `calc(${volume}% - (${volume / 100 - 1} * 20px) - 7.5px)`,
          }}
        >
          <p>{volume}%</p>
        </div>
      </div>
    </div>
  );
}

function SideBar() {
  return (
    <div className="sidebar__content">
      <div className="sidebar__profile">
        <Avatar src="/default_avatar.png" />
        <h3>Your Name</h3>
        <img className="sidebar__profile__cog" src="/cog.svg" alt="Settings" />
      </div>
      <hr className="sidebar__separator" />
      <Search />
      <div className="sidebar__playing">
        <h3>Currently Playing</h3>
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        >
          <VolumeControl />
          <PlayPauseToggle />
        </Track>
        <SongDuration duration={180} startTime={75} />
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        />
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        />
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        />
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        />
        <Track
          name="Song name"
          artist="Artist name"
          thumbnail="/albums/justice/cross/cover.jpg"
        />
      </div>
    </div>
  );
}
