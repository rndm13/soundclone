import "./avatar.scss"

export default function Avatar({src}: {src: string}) {
    return (
        <img src={src} alt="Avatar" className="avatar" />
    );
}
