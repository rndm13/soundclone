import './logo.scss'
export default function Logo() {
  return <img src="/logo.svg" alt="A starfall logo" />;
}
